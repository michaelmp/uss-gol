(function() {
  'use strict';

  function mouseMove(e) {
    relPos(e);
  }

  function mouseUp(e) {
    relPos(e);
  }

  function mouseDown(e) {
    var mousePos = relPos(e);

    console.log('X: ' + mousePos.x + ' Y: ' + mousePos.y);
  }

  /**
   * Returns the mouse position relative to the canvas.
   *
   * @param <MouseEvent> e
   * @return {x, y}
   */
  function relPos(e) {
    // if (e.offsetX) {
    //   return {
    //     x: e.offsetX,
    //     y: e.offsetY
    //   };
    // }

    var offsetX = 0;
    var offsetY = 0;
    var current = e.target;

    do {
      offsetX += current.offsetLeft - current.scrollLeft;
      offsetY += current.offsetTop - current.scrollTop;
    } while (current = current.offsetParent);

    return {
      x: e.pageX - offsetX,
      y: e.pageY - offsetY
    };
  }

  this.Life = {
    init: function(canvas, scale, width, height) {
      var ctx = canvas.getContext('2d');

      canvas.width = scale * width + 1;
      canvas.height = scale * height + 1;

      canvas.addEventListener("mousedown", mouseDown, false);
      canvas.addEventListener("mouseup", mouseUp, false);
      canvas.addEventListener("mousemove", mouseMove, false);

      ctx.translate(0.5, 0.5);

      var grid = new Grid(width, height);

      this.drawGrid(ctx, grid, scale);
    },

    drawGrid: function(ctx, grid, scale) {
      ctx.strokeStyle = Styles.grid;

      for (var i = 0; i < grid.height; i++) {
        for (var j = 0; j < grid.width; j++) {
          ctx.lineWidth = 1;
          ctx.strokeRect(i * scale, j * scale, scale, scale);
        }
      }
    },
  };

  this.Styles = {
    grid: 'rgb(216,216,216)',
    cell: 'rgb(155,197,221)'
  }

  /**
   * Constructs an empty grid
   *
   * @param <Integer> width
   * @param <Integer> height
   */
  function Grid(width, height) {
    this.width = width,
    this.height = height,

    this.cells = new Array(width);

    for (var i = 0; i < width; i++) {
      this.cells[i] = new Array(height);
    }
  }
}).call(this);
